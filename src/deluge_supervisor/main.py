#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
This is a skeleton file that can serve as a starting point for a Python
console script. To run this script uncomment the following lines in the
[options.entry_points] section in setup.cfg:

    console_scripts =
         fibonacci = deluge_supervisor.main:run

Then run `python setup.py install` which will install the command `fibonacci`
inside your current environment.
Besides console scripts, the header (i.e. until _logger...) of this file can
also be used as template for Python modules.

Note: This skeleton file can be safely removed if not needed!
"""

import argparse
import os
import subprocess
import sys
import logging

from deluge_supervisor import __version__

__author__ = "gkmcd"
__copyright__ = "gkmcd"
__license__ = "mit"

_logger = logging.getLogger(__name__)

#_TORRENT_DIR = "/torrent/downloads/deluge-complete"
_TORRENT_DIR = "/home/torrent/downloads/deluge-complete"
_SERVER_IP = "127.0.0.1"
_SERVER_PORT = "58846"
_SERVER_USER = "localclient"
_SERVER_PASS = "24f11a10c68975b0a61478b4b15127dbda39ebf9"


class BaseError(Exception):
    """."""


class BadDirectoryError(Exception):
    """."""


class DirectoryNotEmptyError(Exception):
    """."""


def deluge_start():
    """."""
    # build command string
    # deluge-console "connect 127.0.0.1:##### ; add $(TorrentPathName) ; exit"
    cmd = 'connect {}:{} {} {}; resume 1 ; exit'.format(_SERVER_IP,
                                                        _SERVER_PORT,
                                                        _SERVER_USER,
                                                        _SERVER_PASS)
    subprocess.run(['deluge-console', cmd], stdout=subprocess.PIPE)


def parse_args(args):
    """Parse command line parameters

    Args:
      args ([str]): command line parameters as list of strings

    Returns:
      :obj:`argparse.Namespace`: command line parameters namespace
    """
    parser = argparse.ArgumentParser(
        description="Just a Fibonnaci demonstration")
    parser.add_argument(
        '--version',
        action='version',
        version='deluge-supervisor {ver}'.format(ver=__version__))
    """
    parser.add_argument(
        dest="n",
        help="n-th Fibonacci number",
        type=int,
        metavar="INT")
    """
    parser.add_argument(
        '-v',
        '--verbose',
        dest="loglevel",
        help="set loglevel to INFO",
        action='store_const',
        const=logging.INFO)
    parser.add_argument(
        '-vv',
        '--very-verbose',
        dest="loglevel",
        help="set loglevel to DEBUG",
        action='store_const',
        const=logging.DEBUG)
    return parser.parse_args(args)


def setup_logging(loglevel):
    """Setup basic logging

    Args:
      loglevel (int): minimum loglevel for emitting messages
    """
    logformat = "[%(asctime)s] %(levelname)s:%(name)s:%(message)s"
    logging.basicConfig(level=loglevel, stream=sys.stdout,
                        format=logformat, datefmt="%Y-%m-%d %H:%M:%S")


def main(args):
    """Main entry point allowing external calls

    Args:
      args ([str]): command line parameter list
    """
    args = parse_args(args)
    setup_logging(args.loglevel)
    _logger.info("Starting deluge_supervisor.")  # include arguments

    # script is run nightly
    # check if completed torrents dir is empty
    _logger.debug("Checking completed torrent directory...")

    try:
        if not os.path.isdir(_TORRENT_DIR):
            raise BadDirectoryError('not a dir')

        if os.listdir(_TORRENT_DIR):
            raise DirectoryNotEmptyError('torrent dir not empty')

    except BadDirectoryError as err:
        raise err
    except DirectoryNotEmptyError as err:
        raise err
    except FileNotFoundError:
        raise BadDirectoryError('torrent dir does not exist')

    deluge_start()

    _logger.info("deluge_supervisor finished.")


def run():
    """Entry point for console_scripts
    """
    main(sys.argv[1:])


if __name__ == "__main__":
    run()
